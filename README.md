# HOWTO

## Linux
 - export THERA_WEBHOOK="%webhook url%"
 - pip install -r requirements.txt
 - python main.py

## Windows (PowerShell)
 - $env:THERA_WEBHOOK = "%webhook url%"
 - pip install -r requirements.txt
 - python main.py
